<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Repository\ReponseRepository;
use App\Repository\QuestionRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QuizzController extends AbstractController
{

    #[Route('/quizz', name: 'quizz')]
    public function index(ReponseRepository $reponseRepository ,QuestionRepository $questionRepository): Response
    {
        // dd($_POST['typeQuiz']);
        $title = 'votre quizz ici';
        $listeQuestion = [];
        // dd($styleQuestion);
        // switch($_POST['typeQuiz']){
        //     case 1:
        //         $nomQuiz = 'Photoshop';
        //         break;
        //     case 2:
        //         $nomQuiz = 'Excel';
        //         break;
        //     case 3:
        //         $nomQuiz = 'Word';
        //         break;
        // }

        $listeQuestion = $this->generateurQuizz();

        $test = $reponseRepository->findall();

        $allQuestionByTypeQuiz[] = $questionRepository->question($_POST['typeQuiz']); // retour de la méthode $_POST
        
        $idQuestion = $allQuestionByTypeQuiz[0][0]["idQuestion"]; //? [0][mon numero de table][idQuestion] id question me permetra de récupéré les question 
        $allReponseByIdQuestion = $reponseRepository->findall();



    //todo ------------------- selectionner tout les choix selon la question 

        $idQuestion = [];
        for($i=0; $i<20; $i++)
        {
            $question[] = $allQuestionByTypeQuiz[0][$listeQuestion[$i]]["intitulerQuestion"];
            $idQuestion[] = $allQuestionByTypeQuiz[0][$listeQuestion[$i]]["idQuestion"];
        }


    //todo---------------- Création d'un tableau multi dimentionnelle 
        $tableauTest = ['choix&Valeur' =>[
                                            'reponse' => [
                                                'reponse1' => [] ,
                                                'reponse2' => [] ,
                                                'reponse3' => [] ,
                                                'reponse4' => [] ,
                                            ] ,
                                            'value' => [
                                                'valueRep1' => [],
                                                'valueRep2' => [],
                                                'valueRep3' => [],
                                                'valueRep4' => [],
                                            ]
                                        ],
                        'question&Type' =>[
                                            'question' => [],
                                            'typeQuestion' => []
                                        ]
                                        ];

//?------------ tableau IdQuestion -----------------\\
for($i = 0 ; $i < count($question) ; $i++){
    $test = $questionRepository->findBy(array('intitulerQuestion' => $question[$i]));
    // dump($test);
    $valeurIdQuestion[] = $test[0]->getidQuestion();
}
//?-------------------------------------------------\\


        //*------------------ Boucle Sur les Question  ------------------- */
for($i = 0 ; $i < count($question) ; $i++){


    }
    //*-------------------------------------------------------------- */
        $valueEtReponse1 = $reponseRepository->findBy(array('choix'=>'choix 1','idQuestion' => 0));
        // dd($valueEtReponse1);
    //todo-------------- boucle pour remplire le tableau
        for($i=0; $i<count($listeQuestion); $i++)
        {
            $typeQuestion = $reponseRepository->findBy(array('idQuestion' => $valeurIdQuestion[$i]));
            $typeQuestionById = $typeQuestion[0]->getidQuestion()->getidTypeQuestion()->getidTypeQuestion();

            $test2 = $reponseRepository->findBy(array('idQuestion' => $valeurIdQuestion[$i]));

            $tableauTest['question&Type']["question"][$i] = $allQuestionByTypeQuiz[0][$listeQuestion[$i]]["intitulerQuestion"]; //récupération intituler question 
            
            $testRetour2 = $questionRepository->findBy(array('intitulerQuestion' => $question[$i]));
            $tableauTest['question&Type']['typeQuestion'][$i] = $testRetour2[0]->getidTypeQuestion()->getidTypeQuestion();
            
            $tableauTest['choix&Valeur']['reponse']['reponse1'][$i] = $test2[0]->getchoix();
            $tableauTest['choix&Valeur']['value']['valueRep1'][$i] = $test2[0]->getvaleurChoix();

            $tableauTest['choix&Valeur']['reponse']['reponse2'][$i] = $test2[1]->getchoix();
            $tableauTest['choix&Valeur']['value']['valueRep2'][$i] = $test2[1]->getvaleurChoix();

            $tableauTest['choix&Valeur']['reponse']['reponse3'][$i] = $test2[2]->getchoix();
            $tableauTest['choix&Valeur']['value']['valueRep3'][$i] = $test2[2]->getvaleurChoix();

            $tableauTest['choix&Valeur']['reponse']['reponse4'][$i] = $test2[3]->getchoix();
            $tableauTest['choix&Valeur']['value']['valueRep4'][$i] = $test2[3]->getvaleurChoix();
        }
        // dd($tableauTest);
        // dd($tableauTest['question&Type']);

        //*********************************************************************** */


//!----------- dump array -------------------------------------------/

//!------------------------------------------------------------------/

//!------------return true et false en string---------------/

        // if ($valueChoix1 === true) {
        //     $valueChoix1 = "true";
        // } elseif ($valueChoix1 === false) {
        //     $valueChoix1 = "false";
        // }
        // if ($valueChoix2 === true) {
        //     $valueChoix2 = "true";
        // } elseif ($valueChoix2 === false) {
        //     $valueChoix2 = "false";
        // }
        // if ($valueChoix3 === true) {
        //     $valueChoix3 = "true";
        // } elseif ($valueChoix3 === false) {
        //     $valueChoix3 = "false";
        // }
        // if ($valueChoix4 === true) {
        //     $valueChoix4 = "true";
        // } elseif ($valueChoix4 === false) {
        //     $valueChoix4 = "false";
        // }
        // dd($question);
//!---------------------------------------------------------/
//?---------------------------------------------------------/
$typeQuiz = rand(1,4);
//?---------------------------------------------------------/

        return $this->render('quizz/quizz.html.twig', [
            'controller_name' => 'QuizzController',
            'title' => $title,
            'type_quiz' => $typeQuiz,
            'question' => $question,
            // 'choix1' => $choix1,
            // 'valueChoix1' => $valueChoix1,
            // 'choix2' => $choix2,
            // 'valueChoix2' => $valueChoix2,
            // 'choix3' => $choix3,
            // 'valueChoix3' => $valueChoix3,
            // 'choix4' => $choix4,
            // 'valueChoix4' => $valueChoix4,
            // 'function' => $function,
            // 'chiffreTest' => $x, // a supprimer
            // 'tableau' => $questionFull,
            // 'test' => $test,
            'tableReponseEtValeur' => $tableauTest
            // 'nomDeLaValeurQuiz' => $nomQuiz
        ]);
    }

    
    //todo ---------- function for generator number question 

    private function inArray($needle, $haystack) 
    {
        $length = count($haystack);
            for($i = 0 ; $i < $length ; $i++ ) {
                if($haystack[$i] == $needle) return true;
            }
        return false;
    }

    private function ajout()
    {
        $chiffreRandome = rand(1,39);
        return $chiffreRandome;
    }

    private function generateurQuizz()
    {
        $listeQuestion = [];
        $nombreQuestion = 20;
        while( count($listeQuestion) < $nombreQuestion )
        {
            $nombre = $this->ajout();
            if(!$this->inArray($nombre, $listeQuestion)) $listeQuestion[] = $nombre ;
        }
        return $listeQuestion;
    }
    

    //todo -------- end fonction 
}
