<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuizzendController extends AbstractController
{
    #[Route('/quizzend', name: 'quizzend')]
    public function index(): Response
    {

        $title = 'Fin De Quizz';
        $test = $_POST['note'];
        $typeQuiz = $_POST['typeQuizz'];
        
        return $this->render('quizzend/quizzend.html.twig', [
            'controller_name' => 'QuizzendController',
            'title' => $title,
            'test' => $test,
            'typeQuiz' => $typeQuiz
        ]);
    }
}
