<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WordController extends AbstractController
{
    #[Route('/word', name: 'word')]
    public function index(): Response
    {
        $title = 'InfoWord';

        return $this->render('word/word.html.twig', [
            'controller_name' => 'WordController',
            'title' => $title,
        ]);
    }
}
