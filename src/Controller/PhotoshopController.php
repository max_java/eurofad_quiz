<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhotoshopController extends AbstractController
{
    #[Route('/photoshop', name: 'photoshop')]
    public function index(): Response
    {
        $title = 'InfoPhotoshop';

        return $this->render('photoshop/photoshop.html.twig', [
            'controller_name' => 'PhotoshopController',
            'title' => $title,
        ]);
    }
}
