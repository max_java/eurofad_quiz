<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExcelController extends AbstractController
{
    #[Route('/excel', name: 'excel')]
    public function index(): Response
    {
        $title = 'InfoExcel';

        return $this->render('excel/excel.html.twig', [
            'controller_name' => 'ExcelController',
            'title' => $title,
        ]);
    }
}