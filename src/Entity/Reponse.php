<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse
 *
 * @ORM\Table(name="reponse", indexes={@ORM\Index(name="reponse_question_FK", columns={"id_question"})})
 * @ORM\Entity(repositoryClass= "App\Repository\ReponseRepository")
 */
class Reponse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_reponse", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idReponse;

    /**
     * @var string
     *
     * @ORM\Column(name="choix", type="string", length=100, nullable=false)
     */
    private $choix;

    /**
     * @var bool
     *
     * @ORM\Column(name="valeur_choix", type="boolean", nullable=false)
     */
    private $valeurChoix;

    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_question", referencedColumnName="id_question")
     * })
     */
    private $idQuestion;

    public function getIdReponse(): ?int
    {
        return $this->idReponse;
    }

    public function getChoix(): ?string
    {
        return $this->choix;
    }

    public function setChoix(string $choix): self
    {
        $this->choix = $choix;

        return $this;
    }

    public function getValeurChoix(): ?bool
    {
        return $this->valeurChoix;
    }

    public function setValeurChoix(bool $valeurChoix): self
    {
        $this->valeurChoix = $valeurChoix;

        return $this;
    }

    public function getIdQuestion(): ?Question
    {
        return $this->idQuestion;
    }

    public function setIdQuestion(?Question $idQuestion): self
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }


}
