<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeQuiz
 *
 * @ORM\Table(name="type_quiz")
 * @ORM\Entity(repositoryClass= "App\Repository\TypeQuizRepository")
 */
class TypeQuiz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_type_quiz", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTypeQuiz;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_du_quiz", type="string", length=50, nullable=false)
     */
    private $nomDuQuiz;

    public function getIdTypeQuiz(): ?int
    {
        return $this->idTypeQuiz;
    }

    public function getNomDuQuiz(): ?string
    {
        return $this->nomDuQuiz;
    }

    public function setNomDuQuiz(string $nomDuQuiz): self
    {
        $this->nomDuQuiz = $nomDuQuiz;

        return $this;
    }


}
