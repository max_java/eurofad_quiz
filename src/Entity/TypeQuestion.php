<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeQuestion
 *
 * @ORM\Table(name="type_question")
 * @ORM\Entity(repositoryClass= "App\Repository\TypeQuestionRepository")
 */
class TypeQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_type_question", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTypeQuestion;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_type_question", type="string", length=50, nullable=false)
     */
    private $nomTypeQuestion;

    public function getIdTypeQuestion(): ?int
    {
        return $this->idTypeQuestion;
    }

    public function getNomTypeQuestion(): ?string
    {
        return $this->nomTypeQuestion;
    }

    public function setNomTypeQuestion(string $nomTypeQuestion): self
    {
        $this->nomTypeQuestion = $nomTypeQuestion;

        return $this;
    }


}
