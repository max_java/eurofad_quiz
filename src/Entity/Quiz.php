<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Quiz
 *
 * @ORM\Table(name="quiz", indexes={@ORM\Index(name="quiz_type_quiz0_FK", columns={"id_type_quiz"}), @ORM\Index(name="quiz_reponse1_FK", columns={"id_reponse"}), @ORM\Index(name="quiz_utilisateur_FK", columns={"id_utilisateur"})})
 * @ORM\Entity(repositoryClass= "App\Repository\QuizRepository")
 */
class Quiz implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_quiz", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idQuiz;

    /**
     * @var int
     *
     * @ORM\Column(name="resultat_quiz", type="integer", nullable=false)
     */
    private $resultatQuiz;

    /**
     * @var \TypeQuiz
     *
     * @ORM\ManyToOne(targetEntity="TypeQuiz")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_quiz", referencedColumnName="id_type_quiz")
     * })
     */
    private $idTypeQuiz;

    /**
     * @var \Reponse
     *
     * @ORM\ManyToOne(targetEntity="Reponse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_reponse", referencedColumnName="id_reponse")
     * })
     */
    private $idReponse;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_utilisateur", referencedColumnName="id_utilisateur")
     * })
     */
    private $idUtilisateur;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    public function getIdQuiz(): ?int
    {
        return $this->idQuiz;
    }

    public function getResultatQuiz(): ?int
    {
        return $this->resultatQuiz;
    }

    public function setResultatQuiz(int $resultatQuiz): self
    {
        $this->resultatQuiz = $resultatQuiz;

        return $this;
    }

    public function getIdTypeQuiz(): ?TypeQuiz
    {
        return $this->idTypeQuiz;
    }

    public function setIdTypeQuiz(?TypeQuiz $idTypeQuiz): self
    {
        $this->idTypeQuiz = $idTypeQuiz;

        return $this;
    }

    public function getIdReponse(): ?Reponse
    {
        return $this->idReponse;
    }

    public function setIdReponse(?Reponse $idReponse): self
    {
        $this->idReponse = $idReponse;

        return $this;
    }

    public function getIdUtilisateur(): ?Utilisateur
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur(?Utilisateur $idUtilisateur): self
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
 $roles[] = $this->roles;
//instead of
$roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

}
