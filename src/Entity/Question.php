<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question", indexes={@ORM\Index(name="question_type_question0_FK", columns={"id_type_question"}), @ORM\Index(name="question_type_quiz_FK", columns={"id_type_quiz"})})
 * @ORM\Entity(repositoryClass= "App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_question", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idQuestion;

    /**
     * @var string
     *
     * @ORM\Column(name="intituler_question", type="string", length=255, nullable=false)
     */
    private $intitulerQuestion;

    /**
     * @var string
     *
     * @ORM\Column(name="image_question", type="string", length=100, nullable=false)
     */
    private $imageQuestion;

    /**
     * @var \TypeQuiz
     *
     * @ORM\ManyToOne(targetEntity="TypeQuiz")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_quiz", referencedColumnName="id_type_quiz")
     * })
     */
    private $idTypeQuiz;

    /**
     * @var \TypeQuestion
     *
     * @ORM\ManyToOne(targetEntity="TypeQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_type_question", referencedColumnName="id_type_question")
     * })
     */
    private $idTypeQuestion;

    public function getIdQuestion(): ?int
    {
        return $this->idQuestion;
    }

    public function getIntitulerQuestion(): ?string
    {
        return $this->intitulerQuestion;
    }

    public function setIntitulerQuestion(string $intitulerQuestion): self
    {
        $this->intitulerQuestion = $intitulerQuestion;

        return $this;
    }

    public function getImageQuestion(): ?string
    {
        return $this->imageQuestion;
    }

    public function setImageQuestion(string $imageQuestion): self
    {
        $this->imageQuestion = $imageQuestion;

        return $this;
    }

    public function getIdTypeQuiz(): ?TypeQuiz
    {
        return $this->idTypeQuiz;
    }

    public function setIdTypeQuiz(?TypeQuiz $idTypeQuiz): self
    {
        $this->idTypeQuiz = $idTypeQuiz;

        return $this;
    }

    public function getIdTypeQuestion(): ?TypeQuestion
    {
        return $this->idTypeQuestion;
    }

    public function setIdTypeQuestion(?TypeQuestion $idTypeQuestion): self
    {
        $this->idTypeQuestion = $idTypeQuestion;

        return $this;
    }


}
