<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass= "App\Repository\UtilisateurRepository")
 */
class Utilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_utilisateur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUtilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="code_stagiaire", type="string", length=50, nullable=false)
     */
    private $codeStagiaire;

    public function getIdUtilisateur(): ?int
    {
        return $this->idUtilisateur;
    }

    public function getCodeStagiaire(): ?string
    {
        return $this->codeStagiaire;
    }

    public function setCodeStagiaire(string $codeStagiaire): self
    {
        $this->codeStagiaire = $codeStagiaire;

        return $this;
    }


}
