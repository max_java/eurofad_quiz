<?php

namespace App\Repository;

use App\Entity\Reponse;
use App\Entity\Question;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Reponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reponse[]    findAll()
 * @method Reponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReponseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reponse::class,Question::class);
    }

    // /**
    //  * @return Reponse[] Returns an array of Reponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reponse
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function choixByIdQuestion($value)
    {
        return $this->createQueryBuilder('a') //?a est un alia questionEcel
            ->select('a')
            ->select('a.choix','a.valeurChoix')
            ->join(Question::class, "q", Join::WITH, "a.idQuestion = q.idQuestion")
            ->andWhere('a.idQuestion IN (:val)')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
        // dd($query);
    }

    public function valueReponseByIdQuestion($value)
    {
        return $this->createQueryBuilder('a') //?a est un alia questionEcel
            ->select('a.valueChoix')
            ->join(Question::class, "q", Join::WITH, "a.valueChoix = q.idQuestion")
            ->andWhere('q.idQuestion = (:val)')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
        // dd($query);
    }



}
