<?php

namespace App\Repository;

use App\Entity\Question;
use App\Entity\TypeQuestion;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Question::class, TypeQuestion::class);
    }

    // /**
    //  * @return Question[] Returns an array of Question objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Question
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    // public function Question($value)
    // {
    //     return $this->createQueryBuilder('a') //?a est un alia questionEcel
    //         ->select('a')
    //         // ->Join(TypeQuestion::class, "p", Join::WITH, "a.idQuestion = p.idTypeQuestion") // todo tentative de jointure pour récupéré le type de question
    //         ->andWhere('a.idTypeQuiz = (:val)')
    //         ->setParameter('val', $value)
    //         ->getQuery()
    //         ->getResult()
    //     ;
    //     // dd($query);
    // }

        public function Question($value)
    {
        return $this->createQueryBuilder('a') //?a est un alia questionEcel
            ->select('a.intitulerQuestion','a.idQuestion')
            ->andWhere('a.idTypeQuiz = (:val)')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
        // dd($query);
    }
        public function allInfoQuestion($value)
    {
        return $this->createQueryBuilder('a') //?a est un alia questionEcel
            ->select('a')
            ->andWhere('a.idQuestion = (:val)')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
        // dd($query);
    }
}
