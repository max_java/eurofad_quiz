//? le tableau est récupéré dans quizz.html.twig 
let clicks = 0;
const limiteQuestion = 19;
let fini = "quizz fini ";
let note = 0;
let numeroQuestion = 0;
let i = -1;
let x = -1;
// console.log(x);

    const validQuiz = document.getElementById('validerQuiz');
    document.getElementById("buttonVerification").classList.add('d-none');
document.getElementById("buttonFinDeQuiz").classList.add("d-none");
    
    let question = tableauQuestionEtValeur['question&Type']['question'];
    let typeQuestion = tableauQuestionEtValeur['question&Type']['typeQuestion'];
    let reponse1 = tableauQuestionEtValeur['choix&Valeur']['reponse']['reponse1'];
    let value1 = tableauQuestionEtValeur['choix&Valeur']['value']['valueRep1'];
    let reponse2 = tableauQuestionEtValeur['choix&Valeur']['reponse']['reponse2'];
    let value2 = tableauQuestionEtValeur['choix&Valeur']['value']['valueRep2'];
    let reponse3 = tableauQuestionEtValeur['choix&Valeur']['reponse']['reponse3'];
    let value3 = tableauQuestionEtValeur['choix&Valeur']['value']['valueRep3'];
    let reponse4 = tableauQuestionEtValeur['choix&Valeur']['reponse']['reponse4'];
    let value4 = tableauQuestionEtValeur['choix&Valeur']['value']['valueRep4'];

function choixOff()
{
    document.getElementById(`question1_${i}`).disabled = true;
    document.getElementById(`question2_${i}`).disabled = true;
    document.getElementById(`question3_${i}`).disabled = true;
    document.getElementById(`question4_${i}`).disabled = true;
}
    //!       permet de changer la couleur des reponse 

function developpement() {
    choixOff();
    let choix1 = document.getElementById(`question1_${i}`);
    let choix2 = document.getElementById(`question2_${i}`);
    let choix3 = document.getElementById(`question3_${i}`);
    let choix4 = document.getElementById(`question4_${i}`);
        if(value1[i] === true)
        {
            document.getElementById(`question1_${i}`).classList.add('bg-success');
            if(value1[i] && choix1.checked === true) document.getElementById(`choix1_${i}`).classList.add("text-success");
        }
        else if (value1[i] === false && choix1.checked === true)
        {
            document.getElementById(`question1_${i}`).classList.add('bg-danger');
            document.getElementById(`choix1_${i}`).classList.add('text-danger');
        }
    
        if(value2[i] === true)
        {
            document.getElementById(`question2_${i}`).classList.add('bg-success');
            if(value2[i] && choix2.checked === true) document.getElementById(`choix2_${i}`).classList.add('text-success');
        }
        else if(value2[i] === false && choix2.checked === true)
        {
            document.getElementById(`question2_${i}`).classList.add('bg-danger');
            document.getElementById(`choix2_${i}`).classList.add('text-danger');
        }
    
        if(value3[i] === true)
        {
            document.getElementById(`question3_${i}`).classList.add('bg-success');
            if(value3[i] && choix3.checked === true) document.getElementById(`choix3_${i}`).classList.add('text-success');
        }
            else if(value3[i] === false && choix3.checked === true)
        {
            document.getElementById(`question3_${i}`).classList.add('bg-danger');
            document.getElementById(`choix3_${i}`).classList.add('text-danger');
        }
    
        if(value4[i] === true)
        {
            document.getElementById(`question4_${i}`).classList.add('bg-success');
            if(value4[i] && choix4.checked === true) document.getElementById(`choix4_${i}`).classList.add('text-success');
        }
            else if(value4[i] === false && choix4.checked === true)
        {
            document.getElementById(`question4_${i}`).classList.add('bg-danger');
            document.getElementById(`choix4_${i}`).classList.add('text-danger');
        }
}


function verificationTrue(){
    let nombreTrue = 0;
    if(value1[i] === true ) nombreTrue = nombreTrue + 1;
    if(value2[i] === true ) nombreTrue = nombreTrue + 1;
    if(value3[i] === true ) nombreTrue = nombreTrue + 1;
    if(value4[i] === true ) nombreTrue = nombreTrue + 1;
    return nombreTrue;
}
// verificationTrue();
function verificationFalse(){
    let nombreFalse = 0;
    if(value1[i] === false ) nombreFalse = nombreFalse + 1;
    if(value2[i] === false ) nombreFalse = nombreFalse + 1;
    if(value3[i] === false ) nombreFalse = nombreFalse + 1;
    if(value4[i] === false ) nombreFalse = nombreFalse + 1;
    return nombreFalse;
}
function clickVerification() {
    let chiffre = 0;
    let test1 = document.getElementById(`question1_${i}`);
    let test2 = document.getElementById(`question2_${i}`);
    let test3 = document.getElementById(`question3_${i}`);
    let test4 = document.getElementById(`question4_${i}`);
    if (test1.checked == true && value1[i] == true) chiffre = chiffre + 1;
    if (test2.checked == true && value2[i] == true) chiffre = chiffre + 1;
    if (test3.checked == true && value3[i] == true) chiffre = chiffre + 1;
    if (test4.checked == true && value4[i] == true) chiffre = chiffre + 1;
    return chiffre;
}

function suprimmeId() {
    document.getElementById(`question${i+x}`).classList.add("d-none");
}
function generationQuestionByType()
{
                switch (typeQuestion[i]) {
                  //question simple
                  case 1:
                    return (document.getElementById(
                      "question" + i
                    ).innerHTML = ` 
                        <div class="row col-11 mx-auto">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                    <input class="col-2 form-check-input" type="radio" name="question${i}" id="question1_${i}">
                                    <label  class="col-10 ps-2 form-check-label" id="choix1_${i}" for="question1_${i}" >
                                        ${reponse1[i]}
                                    </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                    <input class="col-2 form-check-input" type="radio" name="question${i}" id="question2_${i}">
                                    <label  class="col-10 ps-2 form-check-label" id="choix2_${i}" for="question2_${i}" >
                                        ${reponse2[i]}
                                    </label>
                            </div>
                        </div>

                        <div class="row col-11 mx-auto my-5">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                    <input class="col-2 form-check-input" type="radio" name="question${i}" id="question3_${i}">
                                    <label  class="col-10 ps-2 form-check-label" id="choix3_${i}" for="question3_${i}" >
                                        ${reponse3[i]}
                                    </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                    <input class="col-2 form-check-input" type="radio" name="question${i}" id="question4_${i}">
                                    <label  class="col-10 ps-2 form-check-label" id="choix4_${i}" for="question4_${i}" >
                                        ${reponse4[i]}
                                    </label>
                            </div>
                        </div>
                    `);
                    break;
                  // multiple
                  case 2:
                    return (document.getElementById(
                      "question" + i
                    ).innerHTML = `
                        <div class="row col-11 mx-auto">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question1_${i}">
                                <label class="col-10 ps-2 form-check-label" id="choix1_${i}" for="question1_${i}" >
                                    ${reponse1[i]}
                                </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question2_${i}">
                                <label class="col-10 ps-2 form-check-label" id="choix2_${i}" for="question2_${i}" >
                                    ${reponse2[i]}
                                </label>
                            </div>
                        </div>

                        <div class="row col-11 mx-auto my-5">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question3_${i}">
                                <label class="col-10 ps-2 form-check-label" id="choix3_${i}" for="question3_${i}" >
                                    ${reponse3[i]}
                                </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question4_${i}">
                                <label class="col-10 ps-2 form-check-label" id="choix4_${i}" for="question4_${i}" >
                                    ${reponse4[i]}
                                </label>
                            </div>
                        </div>
                    `);
                    break;
                  // image
                case 3:
                    return (document.getElementById("question" + i).innerHTML =
                    "case 3");
                    break;
                  // mot mystere
                  case 4:
                    return (document.getElementById(
                      "question" + i
                    ).innerHTML = `
                        <div class="row col-11 mx-auto">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input " type="checkbox" name="question${i}" id="question1_${i}">
                                <label class=" col-10 ps-2 form-check-label" id="choix1_${i}" for="question1_${i}" >
                                    ${reponse1[i]}
                                </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question2_${i}">
                                <label class=" col-10 ps-2 form-check-label" id="choix2_${i}" for="question2_${i}" >
                                    ${reponse2[i]}
                                </label>
                            </div>
                        </div>

                        <div class="row col-11 mx-auto my-5">
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question3_${i}">
                                <label class=" col-10 ps-2 form-check-label" id="choix3_${i}" for="question3_${i}" >
                                    ${reponse3[i]}
                                </label>
                            </div>
                            <div class="col-sm-7 mx-auto col-xl-4 mb-5">
                                <input class="col-2 form-check-input" type="checkbox" name="question${i}" id="question4_${i}">
                                <label class=" col-10 ps-2 form-check-label" id="choix4_${i}" for="question4_${i}" >
                                    ${reponse4[i]}
                                </label>
                            </div>
                        </div>
                    `);
                    break;
                }
}

function clickME() {
    i++;
    note = 0;
    numeroQuestion++;
    clicks++;
    generationQuestionByType();

    if (i === 20) {
        // document.getElementById("numeroQuestion").innerHTML = numeroQuestion; //! a supprimer sa noter fin de quiz
        // document.getElementById("validerQuiz").classList.remove("d-none");
        document.getElementById("button").classList.add("d-none");
        document.getElementById("buttonVerification").classList.add("d-none");
        
        // document.getElementById('buttonVerification').classList.add('d-none');
        document.getElementById('buttonFinDeQuiz').classList.remove('d-none');
    } else {
            document.getElementById("questionQuiz").innerHTML = question[i];
            document.getElementById("button").classList.add("d-none");
            document
              .getElementById("buttonVerification")
              .classList.remove("d-none");
        document.getElementById("numeroQuestion").innerHTML = `question n°${numeroQuestion}`;
        document.getElementById(`question${i + x}`).classList.add('d-none');
        document.getElementById(`button`).classList.add('d-none');
        document.getElementById("buttonVerification").classList.remove("d-none");
        generationQuestionByType();
    }
    document.getElementById('note').value = noteVal();
    // document.getElementById('note').innerHTML = noteVal();
}
let valueNote = 0; 


function noteVal() {
    valueNote = valueNote + note;
    return valueNote;
}

function testVerif() {
    verificationTrue();
    verificationFalse();
    developpement();
        // console.log(verificationTrue());
        // console.log("-/-------");
        // console.log(verificationFalse());
        // console.log("-------/-");
        if (verificationTrue() === clickVerification()) note = note + 5;
        // console.log("--- note /--");
        // console.log(note);
        // console.log("--/ note ---");
    noteVal();
    document.getElementById("buttonVerification").classList.add("d-none");
    document.getElementById("button").classList.remove("d-none");
}



function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

